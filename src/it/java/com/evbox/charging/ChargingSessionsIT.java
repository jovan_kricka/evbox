package com.evbox.charging;

import com.evbox.charging.controller.ChargingSessionController;
import com.evbox.charging.dto.ChargingSessionDto;
import com.evbox.charging.dto.ChargingSessionsSummaryDto;
import com.evbox.charging.entity.StatusEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ChargingSessionsIT {

    @Autowired
    private TestRestTemplate restTemplate;
    @Value("${charging.relevantInterval}")
    private int relevantInterval;

    /**
     * This scenario covers following cases:
     * <ul>
     * <li>Charging session is started and asserted.</li>
     * <li>Summary is fetched with 1 started and 0 stopped sessions (1 total).</li>
     * <li>Session is stopped and asserted.</li>
     * <li>Summary is fetched with 0 started and 1 stopped sessions (1 total).</li>
     * <li>Test waits for relevant interval to pass and then fetches summary again.</li>
     * <li>It is asserted that no charging sessions exist in relevant interval.</li>
     * </ul>
     *
     * @throws InterruptedException this should never happen
     */
    @Test
    public void givenStartedAndStoppedSession_summaryFetched_summaryCorrect() throws InterruptedException {

        final List<ChargingSessionDto> originalChargingSessions = getChargingSessions();
        assertTrue(originalChargingSessions.isEmpty());

        final String id = startChargingSession("1");
        List<ChargingSessionDto> updatedChargingSessions = getChargingSessions();

        assertEquals(1, updatedChargingSessions.size());
        assertNotNull(updatedChargingSessions.get(0).getId());
        assertNotNull(updatedChargingSessions.get(0).getStartedAt());
        assertNull(updatedChargingSessions.get(0).getStoppedAt());
        assertEquals(StatusEnum.IN_PROGRESS.name(), updatedChargingSessions.get(0).getStatus());

        assertSummary(1, 1, 0);

        int httpStatus = stopChargingSession(id);
        assertEquals(HttpStatus.OK.value(), httpStatus);
        updatedChargingSessions = getChargingSessions();

        assertNotNull(updatedChargingSessions.get(0).getStoppedAt());
        assertEquals(StatusEnum.FINISHED.name(), updatedChargingSessions.get(0).getStatus());
        assertSummary(1, 0, 1);

        Thread.sleep(relevantInterval * 1_000);
        assertSummary(0, 0, 0);

    }

    /**
     * This scenario covers following case:
     * <li>Stopping of non existing charging session is attempted and failed with 404.</li>
     */
    @Test
    public void givenNonExistingSession_sessionStopIsAttempted_notFoundStatusReturned() {

        int httpStatus = stopChargingSession(UUID.randomUUID().toString());
        assertEquals(HttpStatus.NOT_FOUND.value(), httpStatus);

    }

    /**
     * This scenario covers following cases:
     * <li>New charging session is started.</li>
     * <li>Stopping of already stopped charging session is attempted and failed with 412.</li>
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void givenStoppedSession_sessionStopIsAttempted_preconditionFailedStatusReturned() {

        final String id = startChargingSession("2");

        int httpStatus = stopChargingSession(id);
        assertEquals(HttpStatus.OK.value(), httpStatus);

        httpStatus = stopChargingSession(id);
        assertEquals(HttpStatus.PRECONDITION_FAILED.value(), httpStatus);


    }

    private List<ChargingSessionDto> getChargingSessions() {

        final ResponseEntity<List<ChargingSessionDto>> response = restTemplate.exchange(
                ChargingSessionController.RESOURCE_PATH,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ChargingSessionDto>>() {
                });

        assertTrue(response.getStatusCode().is2xxSuccessful());
        return response.getBody();

    }

    private String startChargingSession(final String stationId) {

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json");
        final HttpEntity<String> httpEntity = new HttpEntity<>("{ \"" + stationId + "\":\"1\" }", httpHeaders);

        final ResponseEntity response = restTemplate.exchange(
                ChargingSessionController.RESOURCE_PATH,
                HttpMethod.POST,
                httpEntity, String.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        final String resourceLocation = response.getHeaders().getLocation().toString();
        return resourceLocation.substring(resourceLocation.lastIndexOf('/') + 1);

    }

    private int stopChargingSession(final String id) {

        final ResponseEntity response = restTemplate.exchange(
                ChargingSessionController.RESOURCE_PATH + "/" + id,
                HttpMethod.PUT,
                null, String.class);

        return response.getStatusCode().value();

    }

    private ChargingSessionsSummaryDto getSummary() {

        final ResponseEntity<ChargingSessionsSummaryDto> response = restTemplate.exchange(
                ChargingSessionController.RESOURCE_PATH + "/summary",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ChargingSessionsSummaryDto>() {
                });

        assertTrue(response.getStatusCode().is2xxSuccessful());
        return response.getBody();

    }

    private void assertSummary(final int total, final int started, final int stopped) {

        final ChargingSessionsSummaryDto summary = getSummary();

        assertEquals(total, summary.getTotalCount());
        assertEquals(started, summary.getStartedCount());
        assertEquals(stopped, summary.getStoppedCount());

    }

}
