package com.evbox.charging.repository;

import static java.util.Comparator.comparing;

import com.evbox.charging.dto.ChargingSessionsSummaryDto;
import com.evbox.charging.entity.ChargingSession;
import com.evbox.charging.entity.StatusEnum;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;

/**
 * Repository that holds {@link ChargingSession}s.
 * <p>This class is thread safe. It's mutable state is composed of two linked lists guarded by an internal lock.
 * Internal lock is used so client side locking is not allowed.</p>
 * <p>Internal state is published via {@link ChargingSession} and {@link ChargingSessionsSummaryDto} objects.
 * Since published objects are immutable, state publishing of this class is considered safe.</p>
 */
@Slf4j
@Repository
public class ChargingSessionRepository {

	private static final String NOT_FOUND_ERROR = "Charging session with id {0} was not found.";
	private static final String ALREADY_STOPPED = "Charging session with id {0} was already stopped.";
	private static final String ALREADY_EXISTS = "Charging session with timestamp {0} was already created.";

	@VisibleForTesting
	final TreeSet<ChargingSession> startedSessions = new TreeSet<>(comparing(ChargingSession::getStartedAt));
	@VisibleForTesting
	final TreeSet<ChargingSession> stoppedSessions = new TreeSet<>(comparing(ChargingSession::getStoppedAt));
	@VisibleForTesting
	final Map<UUID, ChargingSession> allSessions = new ConcurrentHashMap<>();

	private final Object lock = new Object();
	private final int relevantInterval;

	/**
	 * Constructor for injecting configuration property for relevant time interval that will be used for summary.
	 *
	 * @param relevantInterval integer number representing for how many minutes in the past we should retrieve summary
	 */
	ChargingSessionRepository(@Value("${charging.relevantInterval}") final int relevantInterval) {
		this.relevantInterval = relevantInterval;
	}

	/**
	 * Creates new charging session for station with given id.
	 *
	 * @param stationId string representing station id
	 * @return {@link ChargingSession} that was created
	 */
	public ChargingSession create(final String stationId) {

		final UUID id = UUID.randomUUID();
		final ChargingSession chargingSession = ChargingSession.builder()
			.id(id)
			.stationId(stationId)
			.startedAt(LocalDateTime.now())
			.status(StatusEnum.IN_PROGRESS)
			.build();

		synchronized (lock) {

			if (!startedSessions.add(chargingSession)) {
				throwResponseStatusException(MessageFormat.format(ALREADY_EXISTS, chargingSession.getStartedAt()), HttpStatus.CONFLICT);
			}
			allSessions.put(id, chargingSession);

		}

		return chargingSession;

	}

	/**
	 * Stops charging session with given id which is currently in progress.
	 *
	 * @param id of charging session to stop
	 * @throws ResponseStatusException if charging session with given id is not found at all, exception with 404 HTTP status is thrown and if charging
	 *                                 session with given id was already stopped, exception with 412 HTTP status is thrown.
	 */
	public void stop(final UUID id) {

		synchronized (lock) {

			final ChargingSession chargingSession = allSessions.get(id);

			if (chargingSession == null) {

				final String message = MessageFormat.format(NOT_FOUND_ERROR, id);
				throwResponseStatusException(message, HttpStatus.NOT_FOUND);

			}

			if (!startedSessions.remove(chargingSession)) {

				final String message = MessageFormat.format(ALREADY_STOPPED, id);
				throwResponseStatusException(message, HttpStatus.PRECONDITION_FAILED);

			}

			final ChargingSession stoppedChargingSession = chargingSession.toBuilder()
				.stoppedAt(LocalDateTime.now())
				.status(StatusEnum.FINISHED)
				.build();
			stoppedSessions.add(stoppedChargingSession);
			allSessions.put(id, stoppedChargingSession);

		}

	}

	/**
	 * Gets all charging sessions in the repository.
	 *
	 * @return list of {@link ChargingSession}s
	 */
	public List<ChargingSession> getAll() {

		final ImmutableList.Builder<ChargingSession> builder = ImmutableList.builder();
		return builder.addAll(allSessions.values()).build();

	}

	/**
	 * Gets summary of charging sessions for relevant interval in the past.
	 *
	 * @return {@link ChargingSessionsSummaryDto}
	 */
	public ChargingSessionsSummaryDto getSummary() {

		final LocalDateTime fromMoment = LocalDateTime.now().minusSeconds(relevantInterval);
		final Set<ChargingSession> relevantStartedSessions;
		final Set<ChargingSession> relevantStoppedSessions;

		synchronized (lock) {

			relevantStartedSessions = startedSessions.tailSet(ChargingSession.builder().startedAt(fromMoment).build());
			relevantStoppedSessions = stoppedSessions.tailSet(ChargingSession.builder().stoppedAt(fromMoment).build());

		}

		return ChargingSessionsSummaryDto.builder()
			.totalCount(relevantStartedSessions.size() + relevantStoppedSessions.size())
			.startedCount(relevantStartedSessions.size())
			.stoppedCount(relevantStoppedSessions.size())
			.build();
	}

	/**
	 * Throws {@link ResponseStatusException} with given message and HTTP status.
	 *
	 * @param message    string representing the error message
	 * @param httpStatus {@link HttpStatus}
	 * @throws ResponseStatusException with given message and status
	 */
	private void throwResponseStatusException(final String message, final HttpStatus httpStatus) {

		log.warn(message);
		throw new ResponseStatusException(httpStatus, message);

	}

}
