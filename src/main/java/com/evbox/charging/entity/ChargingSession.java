package com.evbox.charging.entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Charging session entity.
 */
@Getter
@Builder(toBuilder = true)
@EqualsAndHashCode
public class ChargingSession {

    private final UUID id;
    private final String stationId;
    private final LocalDateTime startedAt;
    private final LocalDateTime stoppedAt;
    private final StatusEnum status;

}
