package com.evbox.charging.entity;

/**
 * Enum representing the status of charging sessions.
 */
public enum StatusEnum {

    IN_PROGRESS,
    FINISHED

}
