package com.evbox.charging.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;

/**
 * Data transfer object that models charging sessions summary.
 * NOTE: {@link JsonDeserialize} is not needed for service implementation, but it is needed for integration tests thus
 * it is there.
 */
@Builder
@Getter
@JsonDeserialize(builder = ChargingSessionsSummaryDto.ChargingSessionsSummaryDtoBuilder.class)
public class ChargingSessionsSummaryDto {

    @JsonProperty
    private final int totalCount;
    @JsonProperty
    private final int startedCount;
    @JsonProperty
    private final int stoppedCount;

}
