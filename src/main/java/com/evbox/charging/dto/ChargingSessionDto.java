package com.evbox.charging.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;

/**
 * Data transfer object that models charging session.
 */
@Getter
@Builder
@JsonDeserialize(builder = ChargingSessionDto.ChargingSessionDtoBuilder.class)
public class ChargingSessionDto {

    @JsonProperty
    private final String id;
    @JsonProperty
    private final String stationId;
    @JsonProperty
    private final String startedAt;
    @JsonProperty
    private final String stoppedAt;
    @JsonProperty
    private final String status;

}
