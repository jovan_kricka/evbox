package com.evbox.charging.transformer;

import com.evbox.charging.dto.ChargingSessionDto;
import com.evbox.charging.entity.ChargingSession;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Responsible for transforming {@link ChargingSession} entities to {@link ChargingSessionDto} data transfer objects.
 */
@Component
public class ChargingSessionToDtoTransformer implements Function<ChargingSession, ChargingSessionDto> {

    /**
     * Transforms {@link ChargingSession} to {@link ChargingSessionDto}.
     *
     * @param chargingSession {@link ChargingSession}
     * @return {@link ChargingSessionDto}
     */
    @Override
    public ChargingSessionDto apply(final ChargingSession chargingSession) {

        return ChargingSessionDto.builder()
                .id(chargingSession.getId().toString())
                .stationId(chargingSession.getStationId())
                .startedAt(chargingSession.getStartedAt().toString())
                .stoppedAt(chargingSession.getStoppedAt() == null ? null : chargingSession.getStoppedAt().toString())
                .status(chargingSession.getStatus().name())
                .build();

    }

}
