package com.evbox.charging.service;

import com.evbox.charging.dto.ChargingSessionDto;
import com.evbox.charging.dto.ChargingSessionsSummaryDto;
import com.evbox.charging.entity.ChargingSession;
import com.evbox.charging.repository.ChargingSessionRepository;
import com.evbox.charging.transformer.ChargingSessionToDtoTransformer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service that stores and retrieves data from and to {@link ChargingSessionRepository}.
 * Data from repository is transformed via {@link ChargingSessionToDtoTransformer} to data transfer objects.
 */
@Service
@AllArgsConstructor
public class ChargingSessionService {

    private final ChargingSessionRepository repository;
    private final ChargingSessionToDtoTransformer transformer;

    /**
     * Creates new charging session for station with given id and returns its id.
     *
     * @param stationId string representing station id
     * @return {@link UUID} id of newly created charging session
     */
    public UUID create(final String stationId) {

        final ChargingSession chargingSession = repository.create(stationId);
        return chargingSession.getId();

    }

    /**
     * Stops charging session with given id.
     *
     * @param id {@link UUID} id of charging session to stop
     */
    public void stop(final UUID id) {
        repository.stop(id);
    }

    /**
     * Gets all charging sessions from the repository, transforms them to data transfer objects and returns them.
     *
     * @return list of {@link ChargingSessionDto}s
     */
    public List<ChargingSessionDto> getAll() {

        final List<ChargingSession> chargingSessions = repository.getAll();
        return chargingSessions.stream()
                .map(transformer)
                .collect(Collectors.toList());

    }

    /**
     * Gets summary of recent charging sessions.
     *
     * @return {@link ChargingSessionsSummaryDto}
     */
    public ChargingSessionsSummaryDto getSummary() {
        return repository.getSummary();
    }
}
