package com.evbox.charging.controller;

import com.evbox.charging.dto.ChargingSessionDto;
import com.evbox.charging.dto.ChargingSessionsSummaryDto;
import com.evbox.charging.service.ChargingSessionService;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.UUID;

/**
 * Holds all handlers for charging session REST resource.
 */
@RestController
@RequestMapping(ChargingSessionController.RESOURCE_PATH)
@AllArgsConstructor
public class ChargingSessionController {

    @VisibleForTesting
    public static final String RESOURCE_PATH = "/chargingSessions";
    private static final String RESOURCE_URL_TEMPLATE = "{0}/{1}";

    private final ChargingSessionService service;

    /**
     * Creates new charging session for station with id from given dto.
     *
     * @param dto {@link ChargingSessionDto}
     * @return {@link ResponseEntity}
     */
    @PostMapping
    public ResponseEntity create(final @RequestBody ChargingSessionDto dto) {

        final UUID id = service.create(dto.getStationId());
        final String resourceUrl = MessageFormat.format(RESOURCE_URL_TEMPLATE, RESOURCE_PATH, id);

        return ResponseEntity.created(URI.create(resourceUrl)).build();

    }

    /**
     * Stops charging session with given id.
     *
     * @param id {@link UUID}
     * @return {@link ResponseEntity}
     */
    @PutMapping("/{id}")
    public ResponseEntity stop(final @PathVariable UUID id) {

        service.stop(id);
        final String resourceUrl = MessageFormat.format(RESOURCE_URL_TEMPLATE, RESOURCE_PATH, id);

        return ResponseEntity.ok(URI.create(resourceUrl));

    }

    /**
     * Gets list of all charging sessions.
     *
     * @return {@link ResponseEntity<List<ChargingSessionDto>}
     */
    @GetMapping
    public ResponseEntity<List<ChargingSessionDto>> getAll() {

        final List<ChargingSessionDto> dtos = service.getAll();
        return ResponseEntity.ok(dtos);

    }

    /**
     * Gets a summary of recent charging sessions.
     *
     * @return {@link ResponseEntity<ChargingSessionsSummaryDto>}
     */
    @GetMapping("/summary")
    public ResponseEntity<ChargingSessionsSummaryDto> getSummary() {

        final ChargingSessionsSummaryDto summaryDto = service.getSummary();
        return ResponseEntity.ok(summaryDto);

    }

}
