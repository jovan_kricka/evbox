package com.evbox.charging.service;

import com.evbox.charging.dto.ChargingSessionDto;
import com.evbox.charging.dto.ChargingSessionsSummaryDto;
import com.evbox.charging.entity.ChargingSession;
import com.evbox.charging.repository.ChargingSessionRepository;
import com.evbox.charging.transformer.ChargingSessionToDtoTransformer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ChargingSessionServiceTest {

    private final ChargingSessionRepository repository = mock(ChargingSessionRepository.class);
    private final ChargingSessionToDtoTransformer transformer = mock(ChargingSessionToDtoTransformer.class);

    private final ChargingSessionService service = new ChargingSessionService(repository, transformer);

    @Test
    public void create_sessionViaRepositoryCreated_idReturned() {

        final String stationId = UUID.randomUUID().toString();
        final UUID expectedId = UUID.randomUUID();
        doReturn(ChargingSession.builder().id(expectedId).build()).when(repository).create(eq(stationId));

        final UUID id = service.create(stationId);

        assertEquals(expectedId, id);
        verify(repository, times(1)).create(eq(stationId));

    }

    @Test
    public void stop_sessionViaRepositoryStopped_repositoryCalled() {

        final UUID id = UUID.randomUUID();

        service.stop(id);

        verify(repository, times(1)).stop(eq(id));

    }

    @Test
    public void getAll_sessionsFetchedViaRepositoryAndTransformedViaTransformer_dtoReturned() {

        final List<ChargingSession> chargingSessions = new ArrayList<>();
        chargingSessions.add(ChargingSession.builder().build());
        doReturn(chargingSessions).when(repository).getAll();

        final ChargingSessionDto expectedDto = ChargingSessionDto.builder().build();
        doReturn(expectedDto).when(transformer).apply(eq(chargingSessions.get(0)));

        final List<ChargingSessionDto> dtos = service.getAll();

        assertEquals(1, dtos.size());
        assertEquals(expectedDto, dtos.get(0));
        verify(repository, times(1)).getAll();
        verify(transformer, times(1)).apply(eq(chargingSessions.get(0)));

    }

    @Test
    public void getSummary_summaryViaRepositoryRetrieved_summaryReturned() {

        final ChargingSessionsSummaryDto expectedSummary = ChargingSessionsSummaryDto.builder().build();
        doReturn(expectedSummary).when(repository).getSummary();

        final ChargingSessionsSummaryDto summary = service.getSummary();

        assertEquals(expectedSummary, summary);
        verify(repository, times(1)).getSummary();

    }

}
