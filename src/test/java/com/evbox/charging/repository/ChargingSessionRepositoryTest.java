package com.evbox.charging.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.evbox.charging.dto.ChargingSessionsSummaryDto;
import com.evbox.charging.entity.ChargingSession;
import com.evbox.charging.entity.StatusEnum;

import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.springframework.web.server.ResponseStatusException;

public class ChargingSessionRepositoryTest {

	private static final int RELEVANT_INTERVAL = 60;

	private final ChargingSessionRepository repository = new ChargingSessionRepository(RELEVANT_INTERVAL);

	@Test
	public void create_stationIdGiven_chargingSessionCreated() {

		final String stationId = UUID.randomUUID().toString();

		final ChargingSession chargingSession = repository.create(stationId);

		assertNotNull(chargingSession.getId());
		assertEquals(stationId, chargingSession.getStationId());
		assertNotNull(chargingSession.getStartedAt());
		assertNull(chargingSession.getStoppedAt());
		assertEquals(StatusEnum.IN_PROGRESS, chargingSession.getStatus());

	}

	@Test
	public void stop_existingSessionStopped_sessionMovedFromStartedToStoppedSessions() {

		final ChargingSession chargingSession = repository.create(UUID.randomUUID().toString());

		assertEquals(1, repository.startedSessions.size());
		assertEquals(0, repository.stoppedSessions.size());
		assertEquals(1, repository.allSessions.size());

		repository.stop(chargingSession.getId());

		assertEquals(0, repository.startedSessions.size());
		assertEquals(1, repository.stoppedSessions.size());
		assertEquals(1, repository.allSessions.size());

	}

	@Test(expected = ResponseStatusException.class)
	public void stop_nonExistingSessionStopped_responseStatusExceptionThrown() {
		repository.stop(UUID.randomUUID());
	}

	@Test(expected = ResponseStatusException.class)
	public void stop_alreadyStoppedSessionStopped_responseStatusExceptionThrown() {

		final ChargingSession chargingSession = repository.create(UUID.randomUUID().toString());
		repository.stop(chargingSession.getId());
		repository.stop(chargingSession.getId());

	}

	@Test
	public void getAll_sessionsStartedAndStopped_allSessionsReturned() throws InterruptedException {

		final ChargingSession chargingSession1 = repository.create(UUID.randomUUID().toString());
		Thread.sleep(1);
		final ChargingSession chargingSession2 = repository.create(UUID.randomUUID().toString());
		repository.stop(chargingSession1.getId());

		final List<ChargingSession> allChargingSessions = repository.getAll();

		assertEquals(2, allChargingSessions.size());
		assertTrue(allChargingSessions.stream().anyMatch(chargingSession -> chargingSession.getId().equals(chargingSession1.getId())));
		assertTrue(allChargingSessions.stream().anyMatch(chargingSession -> chargingSession.getId().equals(chargingSession2.getId())));
	}

	@Test
	public void getSummary_sessionsStartedAndStopped_correctSummaryReturned() throws InterruptedException {

		final ChargingSession chargingSession1 = repository.create(UUID.randomUUID().toString());
		Thread.sleep(1);
		repository.create(UUID.randomUUID().toString());
		repository.stop(chargingSession1.getId());

		final ChargingSessionsSummaryDto summary = repository.getSummary();

		assertEquals(2, summary.getTotalCount());
		assertEquals(1, summary.getStartedCount());
		assertEquals(1, summary.getStoppedCount());
	}

}
