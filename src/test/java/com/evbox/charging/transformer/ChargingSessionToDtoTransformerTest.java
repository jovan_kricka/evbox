package com.evbox.charging.transformer;

import com.evbox.charging.dto.ChargingSessionDto;
import com.evbox.charging.entity.ChargingSession;
import com.evbox.charging.entity.StatusEnum;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class ChargingSessionToDtoTransformerTest {

    private final ChargingSessionToDtoTransformer transformer = new ChargingSessionToDtoTransformer();

    @Test
    public void apply_chargingSessionGiven_chargingSessionDtoReturned() {

        final ChargingSession chargingSession = ChargingSession.builder()
                .id(UUID.randomUUID())
                .stationId(UUID.randomUUID().toString())
                .startedAt(LocalDateTime.now())
                .stoppedAt(LocalDateTime.now())
                .status(StatusEnum.FINISHED)
                .build();

        final ChargingSessionDto chargingSessionDto = transformer.apply(chargingSession);

        assertEquals(chargingSession.getId().toString(), chargingSessionDto.getId());
        assertEquals(chargingSession.getStationId(), chargingSessionDto.getStationId());
        assertEquals(chargingSession.getStartedAt().toString(), chargingSessionDto.getStartedAt());
        assertEquals(chargingSession.getStoppedAt().toString(), chargingSessionDto.getStoppedAt());
        assertEquals(chargingSession.getStatus().toString(), chargingSessionDto.getStatus());

    }

}
