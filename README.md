# Charging session tracker

[![N|Solid](https://icon-library.net/images/charging-station-icon/charging-station-icon-19.jpg)](https://icon-library.net/images/charging-station-icon/charging-station-icon-19.jpg)

Charging Sessions Tracker is a spring boot REST service that gives you possibility to start and stop charging sessions and to retrieve information about them.

# Requirements

  - Charging Sessions Tracker requires Maven version 3.5.2
  - It uses Project Lombok for boilerplate code so Lombok plugin is needed for IntelliJ
  - It is built on top of Java 8

# Instructions

  - download code and run **mvn clean install** from root folder to build the project
  - run **java -jar target\charging-x.x.x-SNAPSHOT.jar** to start the service
  - access REST API documentation via http://localhost:8080/swagger-ui.html